import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class RestExample {

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {

			URL url = new URL(
					"https://samples.openweathermap.org/data/2.5/box/city?bbox=12,32,15,37,10&appid=b6907d289e10d714a6e88b30761fae22");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			List<String> list = new ArrayList<>();

			list = br.lines().collect(Collectors.toList());

			// list.stream().forEach(p->System.out.println("for each -->?"+p));
			// //filter(p->p.contains("T"));

			callJSONParser(list.toString());

			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}

	public static void callJSONParser(String output) {
		int count = 0;

		JSONParser parse = new JSONParser();
		try {
			JSONArray jobj = (JSONArray) parse.parse(output);

			JSONObject jsonobj_1 = (JSONObject) jobj.get(0);

			Object newStr = jsonobj_1.get("list");
			JSONArray jobj1 = (JSONArray) parse.parse(newStr.toString());

			for (int i = 0; i < jobj1.size(); i++) {
				JSONObject jsonobj_2 = (JSONObject) jobj1.get(i);
				if (jsonobj_2.get("name").toString().startsWith("T")) {
					count++;
					System.out.println(" cities with parameter \"name\" begins with the letter \"T\" -->  "
							+ jsonobj_2.get("name"));
				}
			}
			System.out.println("number of cities with parameter \"name\" begins with the letter \"T\"-->" + count);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
